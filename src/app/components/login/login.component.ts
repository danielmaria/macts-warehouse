import { Component, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  passwordFormControl = new FormControl('', [Validators.required]);

  submit() {

    const form = new FormGroup({
      email: this.emailFormControl,
      password: this.passwordFormControl,
    });
    
    if (form.valid) {
      console.log(form.value);
    }
  }
  
  @Input() error: string | undefined;

}
