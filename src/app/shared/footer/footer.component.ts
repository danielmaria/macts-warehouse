import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  @Input() currentApplicationVersion = '0.0.0';
  @Input() isProductionEnvironment = false;

  date = new Date();

  constructor() { }

  ngOnInit(): void {
  }

}
